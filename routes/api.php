<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Student APIs
Route::get('/student/get_all_student', 'API\Student@index');
Route::get('/student/get_single_student/{student_id}', 'API\Student@get_single_student');
Route::post('/student/add_student', 'API\Student@add_student');
Route::post('/student/update_student', 'API\Student@update_student');
Route::delete('/student/delete_student/{student_id}', 'API\Student@delete_student');
Route::get('/student/get_all_student_certificates', 'API\Student@get_all_student_certificates');
Route::post('/student/upload_student_certificates', 'API\Student@upload_student_certificates');
Route::delete('/student/delete_student_certificate/{certificate_id}', 'API\Student@delete_student_certificate');


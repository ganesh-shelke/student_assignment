<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentCertificate_model extends Model
{
   public $table = "tbl_student_certificate";

   protected $primaryKey = 'certificate_id';
   public $incrementing = false;
   public $timestamps = false;

   protected $fillable = [
    'certificate_id', 'student_id', 'name', 'path'
   ];
}

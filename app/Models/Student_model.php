<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student_model extends Model
{
   public $table = "tbl_student";

   protected $primaryKey = 'student_id';
   public $incrementing = false;
   public $timestamps = false;

   protected $fillable = [
    'student_id', 'first_name', 'last_name', 'parent_name', 'mobile_no', 'standard', 'course', 'email'
   ];
}

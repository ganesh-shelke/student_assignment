<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Validator;
use App\Models\Student_model;
use App\Models\StudentCertificate_model;

class Student extends Controller
{
   public function __construct()
		{
			header("Access-Control-Allow-Origin: *");
			header("Access-Control-Allow-Methods: POST, GET, PUT, PATCH");
			header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type");      
			header("Access-Control-Max-Age: 86400");
			header('Content-type: application/json');
		}
    // Get all students
    public function index()
		{
			$getAllStudents =  Student_model::select('*')->get();
			if (count($getAllStudents) > 0) {
				  return $this->sendResponse($getAllStudents, 'Data Found.', 200);
			} else {
				  return $this->sendError('Data Not Found.', 0);
			}
		}
    // Get single student
    public function get_single_student($student_id)
		{
			$getAllStudents =  Student_model::select('*')->where('student_id',$student_id)->first();
			if (!is_null($getAllStudents)) {
				  return $this->sendResponse($getAllStudents, 'Data Found.', 200);
			} else {
				  return $this->sendError('Data Not Found.', 0);
			}
		}
    // Add student
    public function add_student(Request $request)
        {
            $output = [];
			$validator = Validator::make($request->all(), [
				'first_name' => 'required',
				'last_name' => 'required',
				'parent_name' => 'required',
				'mobile_no' => 'required',
				'standard' => 'required',
				'course' => 'required'
			]);
			if($validator->fails()){
				return $this->sendError($validator->errors(), 404);
			}
			$input = $request->all();  
            $output['first_name'] = $input['first_name'];
            $output['last_name'] = $input['last_name'];
            $output['parent_name'] = $input['parent_name'];
            $output['mobile_no'] = $input['mobile_no'];
            $output['standard'] = $input['standard'];
            $output['course'] = $input['course'];
            $output['email'] = $input['email'];
            $last_insert_id = Student_model::insertGetId($output);
                    if($last_insert_id > 0) {
                      return $this->sendResponse($last_insert_id, 'Student has been added successfully', 200);
                    } else {
                      return $this->sendError('Student has not added successfully', 0);
                    }
        }
    // Update student
    public function update_student(Request $request)
        {
           $validator = Validator::make($request->all(), [
		   		'student_id' => 'required',
				'first_name' => 'required',
				'last_name' => 'required',
				'parent_name' => 'required',
				'mobile_no' => 'required',
				'standard' => 'required',
				'course' => 'required'
			]);
			if($validator->fails()){
				return $this->sendError($validator->errors(), 404);
			}
			$input = $request->all();
				
            $student_id = $input['student_id'];
            $studentDetails = Student_model::find($student_id);
            if (is_null($studentDetails)) {
                       return $this->sendError('Data Not Found.', 0);
                    }             
                    $studentDetails->first_name = $input['first_name'];
                    $studentDetails->last_name = $input['last_name'];
					$studentDetails->parent_name = $input['parent_name'];
					$studentDetails->mobile_no = $input['mobile_no'];
					$studentDetails->standard = $input['standard'];
					$studentDetails->course = $input['course'];
					$studentDetails->email = $input['email'];
                    $studentDetails->save();
                    if($studentDetails) {
                      return $this->sendResponse($student_id, 'Student has been updated successfully', 200);
                    } else {
                      return $this->sendError('Student has not updated successfully', 0);
                    }
        }
	// Delete student
    public function delete_student($student_id)
        {
            $studentDetails = Student_model::find($student_id);
            if (is_null($studentDetails)) {
                       return $this->sendError('Data Not Found.', 0);
                    }             
			$studentDetails->delete();
			if($studentDetails) {
			  return $this->sendResponse($student_id, 'Student has been deleted successfully', 200);
			} else {
			  return $this->sendError('Student has not deleted successfully', 0);
			}
        }
    // Upload student certificates
    public function upload_student_certificates(Request $request)
        {
            $output = [];
			$validator = Validator::make($request->all(), [
			    'student_id' => 'required',
				'birth_certificate' => 'required|file|max:2048|mimes:jpeg,pdf,png',
				'other_certificate.*' => 'file|max:2048|mimes:jpeg,pdf,png'
			]);
			if($validator->fails()){
				return $this->sendError($validator->errors(), 404);
			}
			$input = $request->all(); 
			$student_id = $input['student_id'];
			$studentCertificateDetails = StudentCertificate_model::where('student_id',$student_id)->get();      
			if($studentCertificateDetails != NULL) {
				foreach($studentCertificateDetails as $row)
					{
					   $path = public_path()."/certificates/".$row->path;
					   if (file_exists($path)) {
				         unlink($path);
						}
					   StudentCertificate_model::find($row->certificate_id)->delete();
					}
			}			
			$birth_certificate = $input['birth_certificate'];
			$other_certificate = [];
			if(array_key_exists('other_certificate', $input)){
				$other_certificate = $input['other_certificate'];
			}
            $destinationPath = './public/certificates';	
			$certificate_data = array();
			if (count((array)$birth_certificate) > 0) {
			   $certificate_imgs = [];
			   $birth_certificate_img = uniqid().'_'.time().'.'.$birth_certificate->getClientOriginalExtension();
			   $birth_certificate->move($destinationPath, $birth_certificate_img);
			   $certificate_imgs['student_id'] = $student_id;
			   $certificate_imgs['path'] = $birth_certificate_img;
			   $certificate_imgs['name'] = 'birth certificate';
			   $certificate_data[] = $certificate_imgs;
			}
			if (count((array)$other_certificate) > 0) {
				 foreach($other_certificate as $row)
					{
						   $certificates = [];
						   $other_certificate_img = '';
						   $other_certificate_img = uniqid().'_'.time().'.'.$row->getClientOriginalExtension();
						   $row->move($destinationPath, $other_certificate_img);
						   $certificates['student_id'] = $student_id;
						   $certificates['path'] = $other_certificate_img;
						   $certificates['name'] = 'other certificate';
						   $certificate_data[] = $certificates;
					}
			}		
            $check_insert_data = StudentCertificate_model::insert($certificate_data);
            if($check_insert_data) {
                      return $this->sendResponse($student_id, 'Student certificates has been added successfully', 200);
                    } else {
                      return $this->sendError('Student certificates has not added successfully', 0);
                    }
        }
    // Get all student certificates
    public function get_all_student_certificates()
		{
			$getAllStudentCertificates =  StudentCertificate_model::select('*')->get();
			if (count($getAllStudentCertificates) > 0) {
				  return $this->sendResponse($getAllStudentCertificates, 'Data Found.', 200);
			} else {
				  return $this->sendError('Data Not Found.', 0);
			}
		}
	// Delete student certificate
    public function delete_student_certificate($certificate_id)
        {
            $studentCertificateDetails = StudentCertificate_model::find($certificate_id);
			$destinationPath = './public/certificates/';
			$image_name = $studentCertificateDetails->path;
            if (is_null($studentCertificateDetails)) {
                       return $this->sendError('Data Not Found.', 0);
                    }             
			$studentCertificateDetails->delete();
			if($studentCertificateDetails) {
				$path = public_path()."/certificates/".$image_name;
				if (file_exists($path)) {
				         unlink($path);
						}
			    return $this->sendResponse($certificate_id, 'Student certificate has been deleted successfully', 200);
			} else {
			  return $this->sendError('Student certificate has not deleted successfully', 0);
			}
        }
    // send success response
    public function sendResponse($result, $message, $status)
      {
        $response = [
              'status' => $status,
              'data'    => $result,
              'message' => $message
          ];
        echo json_encode($response);
      }
    // send error message
    public function sendError($errorMessage, $status)
      {
        $response = [
              'status' => $status,
              'message' => $errorMessage
          ];
        echo json_encode($response);
      }
}